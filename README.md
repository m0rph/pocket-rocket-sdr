![](board/pocket_rocket01.jpg)

<h1>Pocket Rocket Barebones STM32F103 SDR</h1>
Update: A transceiver board has been ordered!
<h1>Pocket Rocket STM32F103 SDR Transceiver</h1>

A tranceiver pcb has been ordered after successful receiver experiments with the Blue Pill board.
The pcb has not been tested yet, once it's put through it's paces the schematic and Gerber filess will be pushed here.
This project is inspired by uSDX and various other barebones QRP SDR transceivers and the [CIRCUIT SALAD 4V QRP AMP](https://circuitsalad.com/2020/11/13/high-efficiency-qrp-amplifier/). The project started quite accidentally and has progressed at variable speeds, it's time to share it. More than anything the Pocket Rocket is an experiment to advance and find new limits of interesting radio software and technological aspects, and hopefully bring together the community with its wisdom. The importance of documentation cannot be underestimated. Keep experimenting and share de OH6UAV.

Here's a list of the Pocket Rocket features in no particular order (mostly untested as of this writing):
- Runs at max 4.2 volts (topped up 18650 cell)
- Has no regulator
- No audio codec, only STM32F103 dual ADC - works well 
- Fixed point arithmetic DSP, no FPU available - works well
- Class D RF power amplifier experiment - not tested, hope to get SSB, CW should work
- Digipot RF fronted AGC driven by ADC watchdog - not tested
- Low noise LCD - works well
- Abundant use of Direct Memory Access (DMA) - works well, leaves CPU time for DSP
- LibOpenCM3 - works well
- Four pi network low pass filter banks operated with a rotary switch - not tested
- USB-C connection - not tested, will not work at 128MHz clock
- To be continued

<h1>Pocket Rocket Barebones STM32F103 SDR Receiver</h1>

I was curious to see the ADC performance of the STM32 family, learn more about digital signal processing, direct memory access and libopencm3. Among other things. The current target is audio quality and selectivity, so no fft spectrum display yet. Currently SSB reception is working. All insight, feedback and help is warmly welcomed.


[![](https://img.youtube.com/vi/oNGwqWnLgM4/0.jpg)](https://www.youtube.com/watch?v=oNGwqWnLgM4 "RX in action")