#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/stm32/flash.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/exti.h>

// in the future we want to have our own taylored dsp functions not cmsis dsp (memory hungry), but it gets the bootstrap job done
// we would also like to test fft ssb and other mode demodulation with fixed point arithmetic, we would then display the spectrum on the display as a bonus
// and since we are bound to the fixed point world, we should use cic decimation, so sick.

#define ARM_MATH_CM3
#include <arm_math.h>
#include <coefficients.h>
#include <pcd8544.h>
#include "si5351.h"
#define SI5351_I2C I2C2

#define ADC_FORW GPIO0   // A
#define ADC_REFL GPIO1   // A
#define ADC_BATV GPIO2   // A
#define PWM_AF GPIO3     // A
#define ADC_RXI GPIO4    // A
#define ADC_RXQ GPIO5    // A
#define PWM_BIAS GPIO6   // A
#define ADC_MIC GPIO7    // A
#define LCD_DC GPIO8     // A
#define PWM_VOL GPIO9    // A
#define LCD_CE GPIO10    // A
#define USBDN GPIO11     // A
#define USBDP GPIO12     // A
#define LCD_RST GPIO15   // A
#define PWM_TXI GPIO0    // B
#define PWM_TXQ GPIO1    // B
#define BTN4 GPIO3       // B
#define BTN3 GPIO4       // B
#define BTN2 GPIO5       // B
#define BTN1 GPIO6       // B
#define ENC_BTN GPIO7    // B
#define ENC_A GPIO8      // B
#define ENC_B GPIO9      // B
#define SLC GPIO10       // B I2C2 si5351
#define SDA GPIO11       // B I2C2 si5351
#define PTT GPIO12       // B
#define LCD_CLK GPIO13   // B
#define LCD_LIGHT GPIO14 // B
#define LCD_DIN GPIO15   // B
#define RX_ON GPIO13     // C
#define ATT_UD GPIO14    // C digipot
#define ATT_INC GPIO15   // C digipot

#define SAMPLE_RATE 68000
#define SAMPLE_COUNT 1024

static int32_t adc_rx_buffer[SAMPLE_COUNT]; // int16 values are stored together as int32 with dual adc
static int16_t pwm_af_buffer[SAMPLE_COUNT];
static int16_t buffer_adc_a[SAMPLE_COUNT / 2];
static int16_t buffer_adc_b[SAMPLE_COUNT / 2];
static int16_t buffer_pwm_a[SAMPLE_COUNT / 2];
static int16_t buffer_pwm_b[SAMPLE_COUNT / 2];
uint16_t adc_rx_buffer_pointer = 0;

int16_t state_right[NO_HILBERT_COEFFS + SAMPLE_COUNT / 2]; // coeff_count + instanace_sample_count
int16_t state_left[NO_HILBERT_COEFFS + SAMPLE_COUNT / 2];  // coeff_count + instanace_sample_count
arm_fir_instance_q15 fir_right, fir_left;

uint32_t frequency = 3663500;
float battery_voltage = 0.00;
char modes[][3] = {"LSB", "USB"};
uint8_t active_mode = 0;
int16_t rot_old_state = 0;
int16_t rot_new_state = 0;
uint16_t frequency_step[] = {10, 100, 500, 1000, 100000};
uint8_t active_step = 2;
uint8_t udpdate_display = true;
uint8_t update_frequency = true;
float volume = 0.22;

static void delay_ms(uint32_t n)
{
    for (size_t i = 0; i < n; i++)
        __asm__("nop");
}

static void clock_setup(void)
{
    rcc_clock_setup_in_hse_8mhz_out_72mhz();
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_DMA1);
    rcc_periph_clock_enable(RCC_ADC1);
    rcc_periph_clock_enable(RCC_ADC2);
    rcc_periph_clock_enable(RCC_I2C2);
    rcc_periph_clock_enable(RCC_AFIO);
    rcc_periph_clock_enable(RCC_TIM1);
    rcc_periph_clock_enable(RCC_TIM2);
    rcc_periph_clock_enable(RCC_TIM3);
    rcc_periph_clock_enable(RCC_TIM4);
    rcc_periph_clock_enable(RCC_SPI2);
}

static void dma_adc_rx_setup(void)
{
    dma_disable_channel(DMA1, DMA_CHANNEL1);
    dma_enable_circular_mode(DMA1, DMA_CHANNEL1);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL1);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_32BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_32BIT);
    dma_set_read_from_peripheral(DMA1, DMA_CHANNEL1);
    dma_set_peripheral_address(DMA1, DMA_CHANNEL1, (uint32_t)&ADC1_DR);
    dma_set_memory_address(DMA1, DMA_CHANNEL1, (uint32_t)&adc_rx_buffer);
    dma_set_number_of_data(DMA1, DMA_CHANNEL1, SAMPLE_COUNT);
    dma_enable_half_transfer_interrupt(DMA1, DMA_CHANNEL1);
    dma_enable_transfer_complete_interrupt(DMA1, DMA_CHANNEL1);
    nvic_enable_irq(NVIC_DMA1_CHANNEL1_IRQ);
    dma_enable_channel(DMA1, DMA_CHANNEL1);
}

static void dma_pwm_af_setup(void)
{
    dma_disable_channel(DMA1, DMA_CHANNEL3);
    dma_enable_circular_mode(DMA1, DMA_CHANNEL3);
    dma_disable_peripheral_increment_mode(DMA1, DMA_CHANNEL3);
    dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL3);
    dma_set_peripheral_size(DMA1, DMA_CHANNEL3, DMA_CCR_PSIZE_16BIT);
    dma_set_memory_size(DMA1, DMA_CHANNEL3, DMA_CCR_MSIZE_16BIT);
    dma_set_read_from_memory(DMA1, DMA_CHANNEL3);
    dma_set_peripheral_address(DMA1, DMA_CHANNEL3, (uint32_t)&TIM2_CCR4);
    dma_set_memory_address(DMA1, DMA_CHANNEL3, (uint32_t)&pwm_af_buffer);
    dma_set_number_of_data(DMA1, DMA_CHANNEL3, SAMPLE_COUNT);
    dma_enable_channel(DMA1, DMA_CHANNEL3);
}

static void adc_rx_setup(void)
{
    gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, ADC_RXI);
    gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_ANALOG, ADC_RXQ);
    adc_power_off(ADC1);
    adc_power_off(ADC2);
    adc_set_dual_mode(ADC_CR1_DUALMOD_RSM);
    adc_set_sample_time(ADC1, 4, ADC_SMPR_SMP_71DOT5CYC);
    adc_set_sample_time(ADC2, 5, ADC_SMPR_SMP_71DOT5CYC);
    adc_enable_external_trigger_regular(ADC1, ADC_CR2_EXTSEL_TIM3_TRGO); // ADC_CR2_EXTSEL_SWSTART - use this for cic filter. calculate sample rate based on adc divider and sample time, and match pwm timer to a fraction of that
    adc_enable_external_trigger_regular(ADC2, ADC_CR2_EXTSEL_TIM3_TRGO); // ADC_CR2_EXTSEL_TIM3_TRGO
    adc_power_on(ADC1);
    adc_power_on(ADC2);
    delay_ms(10);
    adc_reset_calibration(ADC1);
    adc_reset_calibration(ADC2);
    adc_calibrate(ADC1);
    adc_calibrate(ADC2);
    uint8_t ADC1_channel_seq[1] = {4};
    uint8_t ADC2_channel_seq[1] = {5};
    adc_set_regular_sequence(ADC1, 1, ADC1_channel_seq);
    adc_set_regular_sequence(ADC2, 1, ADC2_channel_seq);
    adc_enable_dma(ADC1);
    delay_ms(100);
}

static void pwm_vol_tim1_setup(void)
{
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PWM_VOL);
    timer_set_prescaler(TIM1, 0);
    timer_set_oc_mode(TIM1, TIM_OC2, TIM_OCM_PWM1);
    timer_enable_oc_output(TIM1, TIM_OC2);
    timer_enable_break_main_output(TIM1);
    timer_set_oc_value(TIM1, TIM_OC2, (rcc_ahb_frequency / SAMPLE_RATE) * volume);
    timer_disable_oc_clear(TIM1, TIM_OC2);
    timer_set_period(TIM1, rcc_ahb_frequency / SAMPLE_RATE);
    timer_enable_counter(TIM1);
}

static void pwm_af_tim2_setup(void)
{
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PWM_AF);
    timer_set_prescaler(TIM2, 1);
    timer_set_period(TIM2, rcc_ahb_frequency / SAMPLE_RATE);
    timer_set_oc_mode(TIM2, TIM_OC4, TIM_OCM_PWM1);
    timer_enable_oc_output(TIM2, TIM_OC4);
    timer_enable_counter(TIM2);
}

// static void dma_pwm_tx_setup(void)
// {
//     // we can remove some of channel three setup later as it's defined for rx.
//     // we will start by testing the TIM1 channels individually and then add more.
//     // we will need two dma channels, ch3 is fine as we use tim3_up for rx anyway, but for the other we should try ch6 which maps to tim3_trig
//     // that we don't need another timer. if ch6 doesn't work we can repurpose tim4 with tim4_up on ch7
//     // if the TIM_OC2/3N channels present problems, we can reporpose tim3 for tx with ccr3 and ccr4, the are on the same tx pins.
// }

// static void pwm_tx_tim1_setup(void)
// {
//     gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PWM_TXI); // is this line needed?
//     gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PWM_TXQ); // is this line needed?
//     // we might need to change to alternative function for these pins
//     timer_set_prescaler(TIM1, 0);
//     timer_set_period(TIM1, rcc_ahb_frequency / SAMPLE_RATE);
//     timer_set_oc_mode(TIM1, TIM_OC2N, TIM_OCM_PWM1); // is this doing the gpio_set_mode?
//     timer_set_oc_mode(TIM1, TIM_OC3N, TIM_OCM_PWM1);
//     timer_enable_oc_output(TIM1, TIM_OC2N);
//     timer_enable_oc_output(TIM1, TIM_OC3N);
//     timer_enable_counter(TIM1);
// }

// this timer triggers adc coversions, dma and syncs the buffers to pwm audio out
static void tim3_setup(void)
{
    timer_set_prescaler(TIM3, 1);
    timer_set_period(TIM3, rcc_ahb_frequency / SAMPLE_RATE);
    timer_set_master_mode(TIM3, TIM_CR2_MMS_UPDATE);
    timer_enable_irq(TIM3, TIM_DIER_UDE); // tim3_up channel 3
    timer_enable_counter(TIM3);
}

// oh what we have sacrificed for this badboy (timer). UX first eih?
static void encoder_tim4_setup(void)
{
    // gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO6);
    // gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO7);
    // timer_set_period(TIM4, 88); // amount of encoder clicks for full turn, we probably only want to know ++/-- laterz
    // timer_set_prescaler(TIM4, 2);
    timer_slave_set_mode(TIM4, TIM_SMCR_SMS_EM3);     // TIM_SMCR_SMS_EM3
    timer_ic_set_input(TIM4, TIM_IC1, TIM_IC_IN_TI1); // only ch1 and ch2 work with encoders :(
    timer_ic_set_input(TIM4, TIM_IC2, TIM_IC_IN_TI2);
    timer_ic_set_filter(TIM4, TIM_IC1, TIM_IC_CK_INT_N_4);
    timer_ic_set_filter(TIM4, TIM_IC2, TIM_IC_CK_INT_N_4);
    timer_enable_counter(TIM4);
    // timer_enable_update_event(TIM4);
    // timer_enable_irq(TIM4, TIM_DIER_UIE);
    // nvic_enable_irq(NVIC_TIM4_IRQ);
}

static void i2c_setup(void)
{
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_I2C2_SCL | GPIO_I2C2_SDA);
    i2c_peripheral_disable(I2C2);
    i2c_set_speed(I2C2, i2c_speed_fmp_1m, 36); // we can raise this speed up if needed and try DMA
    i2c_peripheral_enable(I2C2);
}

static void disable_tx(void)
{
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, PWM_TXI | PWM_TXQ);
    gpio_clear(GPIOB, PWM_TXI | PWM_TXQ);
}

// static void mute(void)
// {
//     gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, PWM_VOL);
//     gpio_clear(GPIOA, PWM_VOL);
// }

static void pcd8544_setup(void)
{
    gpio_set_mode(PCD8544_SPI_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, PCD8544_SPI_MOSI | PCD8544_SPI_SCK); //| PCD8544_SPI_SS
    spi_reset(PCD8544_SPI);
    spi_init_master(PCD8544_SPI, SPI_CR1_BAUDRATE_FPCLK_DIV_4, SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, SPI_CR1_CPHA_CLK_TRANSITION_1, SPI_CR1_DFF_8BIT, SPI_CR1_MSBFIRST);
    spi_enable_software_slave_management(PCD8544_SPI);
    spi_set_nss_high(PCD8544_SPI);
    spi_enable(PCD8544_SPI);
    gpio_set_mode(PCD8544_RST_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_RST);
    gpio_set_mode(PCD8544_DC_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_DC);
    gpio_set_mode(PCD8544_SCE_PORT, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, PCD8544_SCE);
    gpio_set(PCD8544_RST_PORT, PCD8544_RST);
    gpio_set(PCD8544_SCE_PORT, PCD8544_SCE);

    for (int i = 0; i < 200; i++)
        __asm__("nop");
}

static void si5351_set_freq_90(uint64_t freq)
{
    uint8_t coef = 650000000 / freq; // 650000000
    uint64_t pll_freq = coef * freq;
    set_freq_manual(freq * 100, pll_freq * 100, SI5351_CLK0);
    set_freq_manual(freq * 100, pll_freq * 100, SI5351_CLK1);
    set_phase(SI5351_CLK0, 0);
    set_phase(SI5351_CLK1, coef);
    pll_reset(SI5351_PLLA);
}

// experiments have shown that this dsp section in isr context is faster than monitoring flag in the main loop, prove me wrong please
void dma1_channel1_isr()
{
    if (dma_get_interrupt_flag(DMA1, DMA_CHANNEL1, DMA_TCIF))
    {
        adc_rx_buffer_pointer = SAMPLE_COUNT / 2;
    }
    if (dma_get_interrupt_flag(DMA1, DMA_CHANNEL1, DMA_HTIF))
    {
        adc_rx_buffer_pointer = 0;
    }
    for (size_t i = 0; i < SAMPLE_COUNT / 2; i++)
    {
        buffer_adc_a[i] = adc_rx_buffer[i + adc_rx_buffer_pointer];
        buffer_adc_b[i] = (adc_rx_buffer[i + adc_rx_buffer_pointer] >> 16);
    }
    arm_fir_fast_q15(&fir_right, buffer_adc_a, buffer_pwm_a, SAMPLE_COUNT / 2);
    arm_fir_fast_q15(&fir_left, buffer_adc_b, buffer_pwm_b, SAMPLE_COUNT / 2);
    arm_add_q15(buffer_pwm_a, buffer_pwm_b, &pwm_af_buffer[adc_rx_buffer_pointer], SAMPLE_COUNT / 2);
    dma_clear_interrupt_flags(DMA1, DMA_CHANNEL1, DMA_TCIF | DMA_HTIF);
}

static void x9c10x_setup()
{
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, ATT_UD);
    gpio_set_mode(GPIOC, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, ATT_INC);
    gpio_clear(GPIOC, ATT_UD);
    for (size_t i = 0; i < 99; i++)
    {
        gpio_set(GPIOC, ATT_INC);
        delay_ms(1);
        gpio_clear(GPIOC, ATT_INC);
        delay_ms(1);
    }
    gpio_set(GPIOC, ATT_UD);
    for (size_t i = 0; i < 15; i++)
    {
        gpio_set(GPIOC, ATT_INC);
        delay_ms(1);
        gpio_clear(GPIOC, ATT_INC);
        delay_ms(1);
    }
    // gpio_clear(GPIOC, ATT_UD);
}

void exti9_5_isr(void)
{
    exti_reset_request(EXTI5);
    exti_reset_request(EXTI6);
    exti_reset_request(EXTI7);
    exti_reset_request(EXTI8);
    exti_reset_request(EXTI9);
    udpdate_display = true;
    rot_new_state = gpio_get(GPIOB, ENC_A); // 768,512,256,0

    if (rot_new_state != rot_old_state && rot_new_state == 256)
    {
        if ((gpio_get(GPIOB, ENC_B) >> 1) != rot_new_state)
        {
            if (gpio_get(GPIOB, ENC_BTN) == 0)
                volume -= 0.01;
            else
            {
                frequency -= frequency_step[active_step];
                update_frequency = true;
                si5351_set_freq_90(frequency);
            }
        }
        else
        {
            if (gpio_get(GPIOB, ENC_BTN) == 0)
                volume += 0.01;
            else
            {
                frequency += frequency_step[active_step];
                update_frequency = true;
                si5351_set_freq_90(frequency);
            }
        }
    }
    rot_old_state = rot_new_state;

    if (gpio_get(GPIOB, BTN1) == 0)
    {

        gpio_toggle(GPIOB, LCD_LIGHT);
    }
    if (gpio_get(GPIOB, BTN2) == 0)
    {

        if (active_mode == 1)
            active_mode = 0;

        else
            active_mode++;
    }
}

void exti3_isr()
{
    exti_reset_request(EXTI3);
}

void exti4_isr(void)
{
    udpdate_display = true;
    if (gpio_get(GPIOB, BTN3) == 0)
    {
        if (active_step == 4)
            active_step = 0;
        else
            active_step++;
    }
    exti_reset_request(EXTI4);
}

static void ui_init()
{
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, LCD_LIGHT);
    nvic_enable_irq(NVIC_EXTI9_5_IRQ);
    nvic_enable_irq(NVIC_EXTI3_IRQ);
    nvic_enable_irq(NVIC_EXTI4_IRQ);
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, ENC_B | ENC_A | ENC_BTN | BTN1 | BTN2 | BTN3 | BTN4);
    exti_select_source(EXTI3, GPIOB);
    exti_select_source(EXTI4, GPIOB);
    exti_select_source(EXTI5, GPIOB);
    exti_select_source(EXTI6, GPIOB);
    exti_select_source(EXTI7, GPIOB);
    exti_select_source(EXTI8, GPIOB);
    exti_select_source(EXTI9, GPIOB);

    exti_set_trigger(EXTI3, EXTI_TRIGGER_FALLING);
    exti_set_trigger(EXTI4, EXTI_TRIGGER_FALLING);
    exti_set_trigger(EXTI5, EXTI_TRIGGER_FALLING);
    exti_set_trigger(EXTI6, EXTI_TRIGGER_FALLING);
    exti_set_trigger(EXTI7, EXTI_TRIGGER_FALLING);
    exti_set_trigger(EXTI8, EXTI_TRIGGER_BOTH);
    exti_set_trigger(EXTI9, EXTI_TRIGGER_BOTH);

    // exti_enable_request(EXTI3);
    exti_enable_request(EXTI4);
    exti_enable_request(EXTI5);
    exti_enable_request(EXTI6);
    exti_enable_request(EXTI7);
    exti_enable_request(EXTI8);
    exti_enable_request(EXTI9);
}

int main(void)
{
    clock_setup();
    x9c10x_setup();
    i2c_setup();
    si5351_init(SI5351_CRYSTAL_LOAD_10PF, 27004000, 0); // 27004500
    si5351_drive_strength(SI5351_CLK0, SI5351_DRIVE_2MA);
    si5351_drive_strength(SI5351_CLK1, SI5351_DRIVE_2MA);
    si5351_set_freq_90(frequency);
    adc_rx_setup();
    dma_adc_rx_setup();
    pwm_vol_tim1_setup();
    pwm_af_tim2_setup();
    dma_pwm_af_setup();
    tim3_setup();
    arm_fir_init_q15(&fir_right, NO_HILBERT_COEFFS, hilbert_plus_2200_68000, state_right, SAMPLE_COUNT / 2);
    arm_fir_init_q15(&fir_left, NO_HILBERT_COEFFS, hilbert_minus_2200_68000, state_left, SAMPLE_COUNT / 2);
    pcd8544_setup();
    pcd8544_init();
    pcd8544_clearDisplay();
    wchar_t buffer[10];
    ui_init();
    while (1)
    {
        // if (update_frequency)
        // {
        //     update_frequency = false;
        //     si5351_set_freq_90(frequency);
        // }
        if (udpdate_display)
        {
            udpdate_display = false;
            timer_set_oc_value(TIM1, TIM_OC2, (rcc_ahb_frequency / SAMPLE_RATE) * volume);
            pcd8544_clearDisplay();
            swprintf(buffer, 10, L"%.2fV", battery_voltage);
            pcd8544_drawText(0, 0, BLACK, buffer);
            swprintf(buffer, 10, L"%.2f", volume);
            pcd8544_drawText(62, 0, BLACK, buffer);
            swprintf(buffer, 20, L"%d", frequency);
            pcd8544_drawText(20, 20, BLACK, buffer);
            swprintf(buffer, 4, L"%s", modes[active_mode]);
            pcd8544_drawText(0, 40, BLACK, buffer);
            swprintf(buffer, 5, L"%d", frequency_step[active_step]);
            pcd8544_drawText(61, 40, BLACK, buffer);
            pcd8544_display();
        }
        delay(1);
    }

    return 0;
}
